﻿namespace Termway.Helper
{
    using System;
    using System.Linq;
    using System.Reflection;
    using System.Text;

    public static class ObjectExtension
    {
        /// <summary>
        /// Parse all properties into a string.
        /// </summary>
        public static string ToPropertiesString(this object obj, string seperator = " : ")
        {
            PropertyInfo[] propertyInfos = obj.GetType().GetProperties();
            StringBuilder sb = new StringBuilder();
            foreach (PropertyInfo propertyInfo in propertyInfos)
                sb.Append(propertyInfo.Name).
                   Append(seperator).
                   AppendLine(propertyInfo.GetValue(obj, null).ToString());

            return sb.ToString();
        }

        public static bool IsIn<T>(this T source, params T[] elements)
        {
            if (null == source)
                throw new ArgumentNullException("source");
            return elements.Contains(source);
        }
    }
}