﻿#if UNITY_EDITOR

namespace Termway.Helper
{
    using System;
    using System.Reflection;
    using UnityEditor;
    using UnityEngine;

    /// <summary>
    /// Usage [OnValueChanged(nameof(Method))]
    /// </summary>
    public class OnValueChangedAttribute : PropertyAttribute
    {
        public string methodName;
        public Type[] parametersType;

        public OnValueChangedAttribute(string methodName)
        {
            this.methodName = methodName;
        }

        public OnValueChangedAttribute(string methodName, Type parameterType) : this(methodName)
        {
            this.parametersType = new Type[] { parameterType };
        }

        public OnValueChangedAttribute(string methodName, Type[] parametersType) : this(methodName)
        {
            this.parametersType = parametersType;
        }

        public Type ParameterType
        {
            get
            {
                return parametersType == null || parametersType.Length == 0 ? null : parametersType[0];
            }
            set
            {
                this.parametersType = new Type[] { value };
            }

        }

    }
    

    [CustomPropertyDrawer(typeof(OnValueChangedAttribute))]
    public class OnValueChangedAttributeDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            OnValueChangedAttribute onValueChangedAttribute = attribute as OnValueChangedAttribute;

            EditorGUI.BeginChangeCheck();
            EditorGUI.PropertyField(position, property);
            if (EditorGUI.EndChangeCheck())
            {
                property.serializedObject.ApplyModifiedProperties();
                MethodInfo methodInfo = GetMethodInfo(fieldInfo.DeclaringType, onValueChangedAttribute);
                if(methodInfo.GetParameters().Length == 0)
                    methodInfo.Invoke(property.serializedObject.targetObject, null);
                else
                    methodInfo.Invoke(property.serializedObject.targetObject,  new object[] { fieldInfo.GetValue(property.serializedObject.targetObject) });
            }
        }


        MethodInfo GetMethodInfo(Type type, OnValueChangedAttribute attribute)
        {
            MethodInfo methodInfo = null;

            methodInfo = type.GetMethod(
                    attribute.methodName,
                    BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance,
                    null,
                    CallingConventions.Any,
                    new Type[] { fieldInfo.FieldType }, null);

            if (methodInfo == null)
            {
                if (attribute.parametersType == null)
                    methodInfo = type.GetMethod(attribute.methodName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

                else
                    methodInfo = type.GetMethod(
                        attribute.methodName,
                        BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance,
                        null,
                        CallingConventions.Any,
                        attribute.parametersType, null);
            }
            return methodInfo;
        }
    }

}

#endif